import 'package:flutter/material.dart';

class MyContainer extends StatelessWidget {
  const MyContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      color: Colors.teal,
      width: double.infinity,
      height: size.width * 0.45,
      margin: EdgeInsets.all( size.width * 0.025 ),
    );
  }
}
