import 'package:flutter/material.dart';
import 'package:responsive/UI/Screens/secondScreen.dart';
import 'package:responsive/UI/Widgets/myContainer.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar( title: Text('Home'),),
      // body: SingleChildScrollView(
      //   scrollDirection: Axis.vertical,
      //   child: Column(
      //     children: [
      //       for(int i = 0 ; i < 10 ; i ++)
      //         MyContainer()
      //     ],
      //   ),
      // )
      body: Center(
        child: ElevatedButton(
          child: Text('go to screen 2'),
          onPressed: (){
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => SecondScreen(/**/)
                )
            );
          },
        ),
      ),
    );
  }

}
