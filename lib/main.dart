import 'package:flutter/material.dart';
import 'package:responsive/UI/Screens/home.dart';
import 'package:responsive/UI/Screens/secondScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      routes: {
        '/second' : (c)=> SecondScreen(),
      },

      home: HomeScreen(),
    );
  }
}
